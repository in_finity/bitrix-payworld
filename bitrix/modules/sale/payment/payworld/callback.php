<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require_once($_SERVER["DOCUMENT_ROOT"] 
    . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("sale");

if (isset($_REQUEST['transaction_id']) 
    && isset($_REQUEST['order_id']) 
    && isset($_REQUEST['order_total'])
    && isset($_REQUEST['payer_email']) 
    && isset($_REQUEST['seller_name']) 
    && isset($_REQUEST['shop_id'])
    && isset($_REQUEST['hash']))
{
    CSalePaySystemAction::InitParamArrays($arOrder, $_REQUEST['order_id']);
    
    $secret_code = CSalePaySystemAction::GetParamValue("SECRET_CODE");
    $email_on_error = CSalePaySystemAction::GetParamValue("EMAIL_ON_ERROR");

    $hash = md5(
          $_REQUEST['order_id'] 
        . $_REQUEST['order_total'] 
        . $_REQUEST['transaction_id'] 
        . $_REQUEST['payer_email'] 
        . $_REQUEST['seller_name'] 
        . $_REQUEST['shop_id'] 
        . $secret_code
    );

    if ($_REQUEST['hash'] == $hash)
    {
        $arOrder = CSaleOrder::GetByID($_REQUEST['order_id']);

        if ($arOrder)
        {
            CSalePaySystemAction::InitParamArrays(
                $arOrder, 
                $arOrder["ID"]
            );

            $strPS_STATUS_DESCRIPTION = ''
                . 'Номер транзакции: '
                . $_REQUEST['transaction_id'] . '; '
                . 'Номер заказа: '
                . $_REQUEST['order_id'] . '; '
                . 'Сумма заказа: '
                . $_REQUEST['order_total'] . '; '
                . 'E-mail плательщика: '
                . $_REQUEST['payer_email'];

            $strPS_STATUS_MESSAGE = ''
                . 'Платёж принят для '
                . $_REQUEST['seller_name'] . '; '
                . ' (идентификатор магазина '
                . $_REQUEST['shop_id']
                . ')';

            $arFields = array(
                "PS_STATUS" => "Y",
                "PS_STATUS_CODE" => "-",
                "PS_STATUS_DESCRIPTION" => $strPS_STATUS_DESCRIPTION,
                "PS_STATUS_MESSAGE" => $strPS_STATUS_MESSAGE,
                "PS_SUM" => $_REQUEST['order_total'],
                "PS_CURRENCY" => "",
                "PS_RESPONSE_DATE" => Date(
                    CDatabase::DateFormatToPHP(
                        CLang::GetDateFormat("FULL", LANG)
                    )
                ),
                "USER_ID" => $arOrder["USER_ID"]
            );

            /* You can comment this code if you want PAYED flag 
               not to be set automatically */
            if ($arOrder["PRICE"] == $_REQUEST['order_total'])
            {
                $arFields["PAYED"] = "Y";
                $arFields["DATE_PAYED"] = Date(
                    CDatabase::DateFormatToPHP(
                        CLang::GetDateFormat("FULL", LANG)
                    )
                );
                $arFields["EMP_PAYED_ID"] = false;
            }

            if (CSaleOrder::Update($arOrder["ID"], $arFields))
            { 
                echo 'Success';
            }
            else
            {
                notify_by_email(
                    $email_on_error, 
                    'Ошибка. Ошибка сохранения данных о платеже.', 
                    'Поступила оплата заказа №' 
                        . $_REQUEST['order_id'] 
                        . ' на сумму '
                        . $_REQUEST['order_total']
                        . ' руб. '
                        . ' Не удалось обновить данные о платеже!'
                );

                header('HTTP/1.1 500 Internal Server Error');
                die('Cannot update information on payment');
            }
        }
        else
        {
            notify_by_email(
                $email_on_error, 
                'Ошибка. Заказ не найден.', 
                'Поступила оплата заказа №' 
                    . $_REQUEST['order_id'] 
                    . ' на сумму '
                    . $_REQUEST['order_total']
                    . ' руб. '
                    . ' Заказ с таким номером не найден!'
            );

            header('HTTP/1.1 500 Internal Server Error');
            die('No order found');
        }
    }
    else
    {
        notify_by_email(
            $email_on_error, 
            'Ошибка. Попытка подмены данных об оплате.', 
            'Поступили данные об оплате с неверным хэшем:'
                . ' заказ №'
                . $_REQUEST['order_id'] 
                . ' на сумму '
                . $_REQUEST['order_total']
                . ' руб. '
        );

        header('HTTP/1.1 500 Internal Server Error');
        die('Wrong hash');
    }
}
else
{
    header('HTTP/1.1 500 Internal Server Error');
    die('Not all parameters');
}

require($_SERVER["DOCUMENT_ROOT"] 
    . "/bitrix/modules/main/include/epilog_after.php");




function notify_by_email($to, $subject, $message)
{
    if ($to)
    {
        $headers  = "From: \"Payworld Error Notifier\" " 
            . "<no-reply@" . $_SERVER['HTTP_HOST'] . ">\r\n"
            . "MIME-Version: 1.0\r\n"
            . "Content-type: text/plain; charset=utf-8\r\n"
            . "Content-Transfer-Encoding: 8bit";

        mail(
            $to,
            "=?utf-8?B?" . base64_encode($subject) . "?=",
            $message,
            $headers
        );
    }
}

?>
