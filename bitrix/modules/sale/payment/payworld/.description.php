<?php
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$psTitle = GetMessage("TITLE");
$psDescription = GetMessage("DESC");

$arPSCorrespondence = array(
        "SERVER" => array(
                "NAME" => GetMessage("SERVER"),
                "DESCR" => GetMessage("SERVER_DESC"),
                "VALUE" => "https://pay-world.ru/paymentsystem/enter/",
                "TYPE" => ""
            ),
        "SELLER_NAME" => array(
                "NAME" => GetMessage("SELLER_NAME"),
                "DESCR" => GetMessage("SELLER_NAME_DESC"),
                "VALUE" => "",
                "TYPE" => ""
            ),
        "SHOP_ID" => array(
                "NAME" => GetMessage("SHOP_ID"),
                "DESCR" => GetMessage("SHOP_ID_DESC"),
                "VALUE" => "",
                "TYPE" => ""
            ),
        "SECRET_CODE" => array(
                "NAME" => GetMessage("SECRET_CODE"),
                "DESCR" => GetMessage("SECRET_CODE_DESC"),
                "VALUE" => "",
                "TYPE" => ""
            ),
        "BUTTON_TEXT" => array(
                "NAME" => GetMessage("BUTTON_TEXT"),
                "DESCR" => GetMessage("BUTTON_TEXT_DESC"),
                "VALUE" => "",
                "TYPE" => ""
            ),
        "EMAIL_ON_ERROR" => array(
                "NAME" => GetMessage("EMAIL_ON_ERROR"),
                "DESCR" => GetMessage("EMAIL_ON_ERROR_DESC"),
                "VALUE" => "",
                "TYPE" => ""
            ),
        "AMOUNT" => array(
                "NAME" => GetMessage("AMOUNT"),
                "DESCR" => GetMessage("AMOUNT_DESC"),
                "VALUE" => "SHOULD_PAY",
                "TYPE" => "ORDER"
            )  
    );

?>
