<?php
global $MESS;

$langvars = array (
    'TITLE'               => 'Payworld',
    'DESC'                => '<br>Оплата через платёжную систему ' 
        . '<a href="http://pay-world.ru">Payworld</a>.' 
        . '<br>Для корректной работы модуля оплаты необходимо указать ' 
        . 'следующие настройки магазина в системе Payworld: ' 
        . '<br /> «Result URL» - ' 
        . '"http://<i>your_site_url</i>/bitrix/modules/sale/payment' 
        . '/payworld/callback.php"</i>, ' 
        . '<br />«Success url» - ' 
        . '"http://<i>your_site_url</i>/personal/payment_results.php",'
        . '<br />«Fail url» - '
        . '"http://<i>your_site_url</i>/personal/payment_results.php".',
    'SERVER'              => 'Адрес страницы оплаты в системе ' 
        . '"Мир платежей"',
    'SERVER_DESC'         => '',
    'SELLER_NAME'         => 'Идентификатор продавца в системе ' 
        . '"Мир платежей"',
    'SELLER_NAME_DESC'    => '',
    'SHOP_ID'             => 'Идентификатор магазина в системе '
        . '"Мир платежей"',
    'SHOP_ID_DESC'        => '',
    'SECRET_CODE'         => 'Секретный код магазина (взять из '
        . 'настроек магазина в профиле',
    'SECRET_CODE_DESC'    => '',
    'BUTTON_TEXT'         => 'Текст на кнопке перехода к оплате',
    'BUTTON_TEXT_DESC'    => '',
    'EMAIL_ON_ERROR'      => 'E-mail адрес(а) для уведомления ' 
        . 'в случае ошибки',
    'EMAIL_ON_ERROR_DESC' => 'Можно указать несколько через запятую.',
    'AMOUNT'              => 'Сумма заказа',
    'AMOUNT_DESC'         => 'Сумма к оплате',
);

function _toUtf8($str)
{
    return iconv('utf-8', LANG_CHARSET, $str);
}
    
$langvars = array_map('_toUtf8', $langvars);
$MESS = array_merge($MESS, $langvars);

?>
