<?php
global $MESS;

$langvars = array (
    'TITLE'               => 'Payworld',
    'DESC'                => '<br />Payment via ' 
        . '<a href="http://pay-world.ru">Payworld</a>.' 
        . '<br>You need to provade correct settings for your shop ' 
        . 'in Payworld system: '
        . '<br />«Result URL»  - '
        . '"http://<i>your_site_url</i>/bitrix/modules/sale/payment'
        . '/payworld/callback.php",'
        . '<br />«Success url» - '
        . '"http://<i>your_site_url</i>/personal/payment_results.php",'
        . '<br />«Fail url» - '
        . '"http://<i>your_site_url</i>/personal/payment_results.php".',
    'SERVER'              => 'Payment system URL',
    'SERVER_DESC'         => '',
    'SELLER_NAME'         => 'Your username in Payworld system',
    'SELLER_NAME_DESC'    => '',
    'SHOP_ID'             => 'Identifier of your shop in Payworld system',
    'SHOP_ID_DESC'        => '',
    'SECRET_CODE'         => 'Secret Code of your shop in Payworld system',
    'SECRET_CODE_DESC'    => '',
    'BUTTON_TEXT'         => 'Button text for paying',
    'BUTTON_TEXT_DESC'    => '',
    'EMAIL_ON_ERROR'      => 'E-mail(s) for notification if error occur',
    'EMAIL_ON_ERROR_DESC' => 'You can specify several e-mails delimited by comma.',
    'AMOUNT'              => 'Order amount',
    'AMOUNT_DESC'         => 'Payment amount',
);

$MESS = array_merge($MESS, $langvars);

?>
